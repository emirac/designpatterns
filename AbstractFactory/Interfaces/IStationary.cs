﻿namespace AbstractFactory.Interfaces
{
    public interface IStationary
    {
        string GetStyle();
    }
}
