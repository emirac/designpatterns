﻿using AbstractFactory.Classes;

namespace AbstractFactory.Interfaces
{
    public interface IStationaryFactory
    {
        Journal CreateJournal();
        Notebook CreateNotebook();
    }
}
