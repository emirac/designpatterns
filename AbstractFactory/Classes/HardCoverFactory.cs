﻿namespace AbstractFactory.Classes
{
    class HardCoverFactory : StationaryFactory
    {
        public override Journal CreateJournal()
        {
            return new HardCoverJournal();
        }

        public override Notebook CreateNotebook()
        {
            return new HardCoverNotebook();
        }
    }
}
