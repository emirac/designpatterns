﻿using AbstractFactory.Interfaces;

namespace AbstractFactory.Classes
{
    public abstract class StationaryFactory : IStationaryFactory
    {
        public abstract Journal CreateJournal();

        public abstract Notebook CreateNotebook();
    }
}
