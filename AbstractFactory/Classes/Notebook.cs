﻿using AbstractFactory.Interfaces;

namespace AbstractFactory.Classes
{
    public abstract class Notebook : IStationary
    {
        public abstract string GetStyle();
    }
}
