﻿namespace AbstractFactory.Classes
{
    public class PaperBackNotebook : Notebook
    {
        public PaperBackNotebook()
        {

        }

        public override string GetStyle()
        {
            return "PaperBack";
        }
    }
}
