﻿namespace AbstractFactory.Classes
{
    public class PaperBackJournal : Journal
    {
        public PaperBackJournal()
        {

        }

        public override string GetStyle()
        {
            return "PaperBack";
        }
    }
}
