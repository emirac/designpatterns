﻿namespace AbstractFactory.Classes
{
    public class HardCoverNotebook : Notebook
    {
        public HardCoverNotebook()
        {

        }

        public override string GetStyle()
        {
            return "HardCover";
        }
    }
}
