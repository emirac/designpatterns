﻿namespace AbstractFactory.Classes
{
    class PaperBackFactory : StationaryFactory
    {
        public override Journal CreateJournal()
        {
            return new PaperBackJournal();
        }

        public override Notebook CreateNotebook()
        {
            return new PaperBackNotebook();
        }
    }
}
