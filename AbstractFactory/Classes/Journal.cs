﻿using AbstractFactory.Interfaces;

namespace AbstractFactory.Classes
{
    public abstract class Journal : IStationary
    {
        public abstract string GetStyle();
    }
}
