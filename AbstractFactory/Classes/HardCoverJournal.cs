﻿using System;

namespace AbstractFactory.Classes
{
    public class HardCoverJournal : Journal
    {
        public HardCoverJournal()
        {

        }

        public override string GetStyle()
        {
            return "HardCover";
        }
    }
}
