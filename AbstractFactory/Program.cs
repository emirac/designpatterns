﻿using AbstractFactory.Classes;
using System;

namespace AbstractFactory
{
    class Program
    {
        static void Main(string[] args)
        {
            FormatResults(new HardCoverFactory());
            FormatResults(new PaperBackFactory());
            Console.ReadLine();
        }

        public static void FormatResults(StationaryFactory factory)
        {
            Journal journal = factory.CreateJournal();
            Notebook notebook = factory.CreateNotebook();
            Console.WriteLine($"This is a {journal.GetType().Name}, it is {journal.GetStyle()}");
            Console.WriteLine($"This is a {notebook.GetType().Name}, it is {notebook.GetStyle()}");
            Console.WriteLine();
        }
    }
}
