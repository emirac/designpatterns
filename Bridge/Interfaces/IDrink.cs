﻿namespace Bridge.Interfaces
{
    public interface IDrink
    {
        void Prepare();
    }
}
