﻿using Bridge.Interfaces;

namespace Bridge.Classes
{
    public abstract class DrinksBridge
    {
        public IDrink Drink { get; set; }

        public DrinksBridge(IDrink drink)
        {
            this.Drink = drink;
        }

        public abstract void OrderDrink();
    }
}
