﻿using Bridge.Interfaces;
using System;

namespace Bridge.Classes
{
    public class Coffee : IDrink
    {
        public void Prepare()
        {
            Console.WriteLine("Preparing coffee");
        }
    }
}
