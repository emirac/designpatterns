﻿using Bridge.Interfaces;
using System;

namespace Bridge.Classes
{
    public class SmallDrinksBridge : DrinksBridge
    {
        public SmallDrinksBridge(IDrink drink) : base(drink)
        {

        }

        public override void OrderDrink()
        {
            Console.WriteLine("Ordered small drink");
            this.Drink.Prepare();
        }
    }
}
