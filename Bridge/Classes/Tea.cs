﻿using Bridge.Interfaces;
using System;

namespace Bridge.Classes
{
    public class Tea : IDrink
    {
        public void Prepare()
        {
            Console.WriteLine("Preparing tea");
        }
    }
}
