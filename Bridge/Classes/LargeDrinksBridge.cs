﻿using Bridge.Interfaces;
using System;

namespace Bridge.Classes
{
    public class LargeDrinksBridge : DrinksBridge
    {
        public LargeDrinksBridge(IDrink drink) : base(drink)
        {

        }

        public override void OrderDrink()
        {
            Console.WriteLine("Ordered large drink");
            this.Drink.Prepare();
        }
    }
}
