﻿using Bridge.Classes;
using System;

namespace Bridge
{
    class Program
    {
        static void Main(string[] args)
        {
            LargeDrinksBridge largeCoffee = new LargeDrinksBridge(new Coffee());
            LargeDrinksBridge largeTea = new LargeDrinksBridge(new Tea());
            SmallDrinksBridge smallCoffee = new SmallDrinksBridge(new Coffee());
            SmallDrinksBridge smallTea = new SmallDrinksBridge(new Tea());

            largeCoffee.OrderDrink();
            largeTea.OrderDrink();
            smallCoffee.OrderDrink();
            smallTea.OrderDrink();

            Console.ReadLine();
        }
    }
}
