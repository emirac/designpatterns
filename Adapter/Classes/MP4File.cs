﻿using Adapter.Interfaces;
using System;

namespace Adapter.Classes
{
    public class MP4File : IMP4File
    {
        public MP4File()
        {

        }

        public void PlayVideo()
        {
            Console.WriteLine("Playing MP4 file");
        }
    }
}
