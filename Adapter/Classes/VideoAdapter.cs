﻿using Adapter.Interfaces;

namespace Adapter.Classes
{
    public class VideoAdapter : IMP4File
    {
        public AVIFile AVIFile { get; set; }

        public VideoAdapter(AVIFile aviFile)
        {
            this.AVIFile = aviFile;
        }

        public void PlayVideo()
        {
            this.AVIFile.PlayVideoFile();
        }
    }
}
