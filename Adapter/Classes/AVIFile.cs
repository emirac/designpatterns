﻿using Adapter.Interfaces;

namespace Adapter.Classes
{
    public class AVIFile : IAVIFile
    {
        public AVIFile()
        {

        }

        public void PlayVideoFile()
        {
            System.Console.WriteLine("Playing AVI file");
        }
    }
}
