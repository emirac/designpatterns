﻿namespace Adapter.Interfaces
{
    public interface IAVIFile
    {
        void PlayVideoFile();
    }
}
