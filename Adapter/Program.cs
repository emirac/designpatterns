﻿using Adapter.Classes;
using System;

namespace Adapter
{
    class Program
    {
        static void Main(string[] args)
        {
            MP4File mp4File = new MP4File();
            AVIFile aviFile = new AVIFile();

            mp4File.PlayVideo();
            new VideoAdapter(aviFile).PlayVideo();
            Console.ReadLine();
        }
    }
}
