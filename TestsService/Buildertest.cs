﻿using Builder.Classes;
using Builder.Constants;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestsService
{
    [TestClass]
    public class BuilderTest
    {
        [TestMethod]
        public void TestApetizer()
        {
            var builder = new MealBuilder();
            builder.AddApetizer();

            var meal = builder.GetResult();
            Assert.IsTrue(meal.Items.Count > 0);

            var apetizer = meal.Items.Find(x => x == MealParts.Apetizer);
            Assert.IsNotNull(apetizer);
        }

        [TestMethod]
        public void TestMeat()
        {
            var builder = new MealBuilder();
            builder.AddMeat();

            var meal = builder.GetResult();
            Assert.IsTrue(meal.Items.Count > 0);

            var meat = meal.Items.Find(x => x == MealParts.Meat);
            Assert.IsNotNull(meat);
        }

        [TestMethod]
        public void TestSalad()
        {
            var builder = new MealBuilder();
            builder.AddSalad();

            var meal = builder.GetResult();
            Assert.IsTrue(meal.Items.Count > 0);

            var salad = meal.Items.Find(x => x == MealParts.Salad);
            Assert.IsNotNull(salad);
        }

        [TestMethod]
        public void TestGarnish()
        {
            var builder = new MealBuilder();
            builder.AddGarnish();

            var meal = builder.GetResult();
            Assert.IsTrue(meal.Items.Count > 0);

            var garnish = meal.Items.Find(x => x == MealParts.Garnish);
            Assert.IsNotNull(garnish);
        }

        [TestMethod]
        public void TestDrink()
        {
            var builder = new MealBuilder();
            builder.AddDrink();

            var meal = builder.GetResult();
            Assert.IsTrue(meal.Items.Count > 0);

            var drink = meal.Items.Find(x => x == MealParts.Drink);
            Assert.IsNotNull(drink);
        }

        [TestMethod]
        public void TestDessert()
        {
            var builder = new MealBuilder();
            builder.AddDesert();

            var meal = builder.GetResult();
            Assert.IsTrue(meal.Items.Count > 0);

            var dessert = meal.Items.Find(x => x == MealParts.Dessert);
            Assert.IsNotNull(dessert);
        }

        [TestMethod]
        public void TestFullMeal()
        {
            var mealDirector = new MealDirector();
            var fullMeal = mealDirector.BuildFullMeal();

            foreach (var part in MealParts.AllMealParts)
            {
                Assert.IsTrue(fullMeal.Items.Contains(part));
            }
        }
    }
}
