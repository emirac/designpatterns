﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Prototype.Classes;

namespace TestsService
{
    [TestClass]
    public class PrototypeTest
    {
        [TestMethod]
        public void TestSweaterPrototype()
        {
            var size = "S";
            var color = "Red";
            var neckline = "V-line";

            var sweater = new Sweater(size, color, neckline);
            var sweaterClone = sweater.Clone() as Sweater;

            Assert.AreEqual(sweaterClone.GetSize(), size);
            Assert.AreEqual(sweaterClone.GetColor(), color);
            Assert.AreEqual(sweaterClone.GetNeckline(), neckline);
        }

        [TestMethod]
        public void TestPantsPrototype()
        {
            var size = "S";
            var color = "Green";
            var rise = "High-rise";

            var pants = new Pants(size, color, rise);
            var pantsClone = pants.Clone() as Pants;

            Assert.AreEqual(pantsClone.GetSize(), size);
            Assert.AreEqual(pantsClone.GetColor(), color);
            Assert.AreEqual(pantsClone.GetRise(), rise);
        }
    }
}
