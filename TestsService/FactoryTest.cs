using Factory.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestsService
{
    [TestClass]
    public class FactoryTest
    {
        [TestMethod]
        public void TestCarFactory()
        {
            var factory = new CarFactory();
            var car = factory.CreateVehicle();

            Assert.IsNotNull(car);
            Assert.IsInstanceOfType(car, typeof(Car));
        }

        [TestMethod]
        public void TestBicycleFactory()
        {
            var factory = new BicycleFactory();
            var bicycle = factory.CreateVehicle();

            Assert.IsNotNull(bicycle);
            Assert.IsInstanceOfType(bicycle, typeof(Bicycle));
        }
    }
}
