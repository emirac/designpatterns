﻿namespace Factory.Interfaces
{
    public interface IVehicle
    {
        int GetWheels();
        int GetSpeed();
    }
}
