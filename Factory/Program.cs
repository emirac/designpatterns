﻿using Factory.Classes;
using Factory.Interfaces;
using System;

namespace Factory
{
    class Program
    {
        static void Main(string[] args)
        {
            FormatResults(new CarFactory());
            FormatResults(new BicycleFactory());
            Console.ReadLine();
        }

        public static void FormatResults(VehicleFactory factory)
        {
            IVehicle vehicle = factory.CreateVehicle();
            Console.WriteLine($"This is a {vehicle.GetType().Name}, it has {vehicle.GetWheels()} wheels and goes {vehicle.GetSpeed()} km/h");
            Console.WriteLine();
        }
    }
}
