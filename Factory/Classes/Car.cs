﻿using Factory.Interfaces;

namespace Factory.Classes
{
    public class Car : IVehicle
    {
        public Car()
        {

        }

        public int GetSpeed()
        {
            return 50;
        }

        public int GetWheels()
        {
            return 4;
        }
    }
}
