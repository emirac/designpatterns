﻿using Factory.Interfaces;

namespace Factory.Classes
{
    public abstract class VehicleFactory
    {
        public abstract IVehicle CreateVehicle();
    }
}
