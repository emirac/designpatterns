﻿using Factory.Interfaces;

namespace Factory.Classes
{
    public class Bicycle : IVehicle
    {
        public Bicycle()
        {

        }

        public int GetSpeed()
        {
            return 15;
        }

        public int GetWheels()
        {
            return 2;
        }
    }
}
