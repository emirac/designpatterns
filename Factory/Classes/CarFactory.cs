﻿using Factory.Interfaces;

namespace Factory.Classes
{
    public class CarFactory : VehicleFactory
    {
        public override IVehicle CreateVehicle()
        {
            return new Car();
        }
    }
}
