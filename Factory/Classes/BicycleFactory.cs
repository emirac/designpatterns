﻿using Factory.Interfaces;

namespace Factory.Classes
{
    public class BicycleFactory : VehicleFactory
    {
        public override IVehicle CreateVehicle()
        {
            return new Bicycle();
        }
    }
}
