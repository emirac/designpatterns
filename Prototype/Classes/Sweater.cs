﻿using Prototype.Interfaces;

namespace Prototype.Classes
{
    public class Sweater : Clothing
    {
        private string Neckline { get; set; }

        public Sweater(string size, string color, string neckline) : base(size, color)
        {
            this.Neckline = neckline;
        }

        public override Clothing Clone()
        {
            return (Sweater)this.MemberwiseClone();
        }

        public string GetNeckline()
        {
            return this.Neckline;
        }
    }
}
