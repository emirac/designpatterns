﻿using Prototype.Interfaces;

namespace Prototype.Classes
{
    public abstract class Clothing : IClothing
    {
        public string Size { get; set; }
        public string Color { get; set; }

        public Clothing(string size, string color)
        {
            this.Size = size;
            this.Color = color;
        }

        public abstract Clothing Clone();

        public string GetSize()
        {
            return this.Size;
        }

        public string GetColor()
        {
            return this.Color;
        }
    }
}
