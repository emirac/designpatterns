﻿using Prototype.Interfaces;

namespace Prototype.Classes
{
    public class Pants : Clothing
    {
        private string Rise { get; set; }

        public Pants(string size, string color, string rise) : base(size, color)
        {
            this.Rise = rise;
        }

        public override Clothing Clone()
        {
            return (Pants)this.MemberwiseClone();
        }

        public string GetRise()
        {
            return this.Rise;
        }
    }
}
