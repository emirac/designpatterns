﻿namespace Prototype.Interfaces
{
    public interface IClothing
    {
        string GetSize();
        string GetColor();
    }
}
