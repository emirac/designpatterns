﻿using Prototype.Classes;
using System;
using System.Collections.Generic;

namespace Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            Sweater sweater = new Sweater("S", "Red", "Crewneck");
            Pants pants = new Pants("S", "Black", "High-waisted");
            FormatResults(sweater);
            FormatResults(pants);
            Console.ReadLine();
        }

        public static void FormatResults(Clothing clothing)
        {
            var items = new List<Clothing>()
            {
                clothing
            };

            var clone1 = clothing.Clone();
            var clone2 = clothing.Clone();
            clone1.Size = "M";
            clone2.Color = "Blue";
            items.Add(clone1);
            items.Add(clone2);

            Console.WriteLine("Clothing items:");
            foreach (var item in items)
            {
                Console.WriteLine($"Size: {item.GetSize()}, Color: {item.GetColor()}");
            }
        }
    }
}
