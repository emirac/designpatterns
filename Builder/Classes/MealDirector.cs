﻿namespace Builder.Classes
{
    public class MealDirector
    {
        private MealBuilder Builder { get; set; }

        public MealDirector()
        {
            this.Builder = new MealBuilder();
        }

        public Meal BuildMainCourse()
        {
            this.Builder.AddMeat()
                .AddGarnish()
                .AddSalad();
            return this.Builder.GetResult();
        }

        public Meal BuildFullMeal()
        {
            this.Builder.AddApetizer()
                .AddMeat()
                .AddGarnish()
                .AddSalad()
                .AddDesert()
                .AddDrink();
            return this.Builder.GetResult();
        }
    }
}
