﻿using Builder.Constants;
using Builder.Interfaces;

namespace Builder.Classes
{
    public class MealBuilder : IMealBuilder
    {
        private Meal Meal { get; set; }

        public MealBuilder()
        {
            this.Reset();
        }

        public void Reset()
        {
            this.Meal = new Meal();
        }

        public Meal GetResult()
        {
            Meal result = this.Meal;
            this.Reset();
            return result;
        }

        public IMealBuilder AddApetizer()
        {
            this.Meal.Items.Add(MealParts.Apetizer);
            return this;
        }

        public IMealBuilder AddDesert()
        {
            this.Meal.Items.Add(MealParts.Dessert);
            return this;
        }

        public IMealBuilder AddDrink()
        {
            this.Meal.Items.Add(MealParts.Drink);
            return this;
        }

        public IMealBuilder AddGarnish()
        {
            this.Meal.Items.Add(MealParts.Garnish);
            return this;
        }

        public IMealBuilder AddMeat()
        {
            this.Meal.Items.Add(MealParts.Meat);
            return this;
        }

        public IMealBuilder AddSalad()
        {
            this.Meal.Items.Add(MealParts.Salad);
            return this;
        }
    }
}
