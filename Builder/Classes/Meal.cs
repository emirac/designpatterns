﻿using System.Collections.Generic;

namespace Builder.Classes
{
    public class Meal
    {
        public List<string> Items { get; set; } = new List<string>();

        public Meal()
        {

        }
    }
}
