﻿namespace Builder.Interfaces
{
    public interface IMealBuilder
    {
        IMealBuilder AddMeat();
        IMealBuilder AddSalad();
        IMealBuilder AddGarnish();
        IMealBuilder AddApetizer();
        IMealBuilder AddDesert();
        IMealBuilder AddDrink();
    }
}
