﻿using System.Collections.Generic;

namespace Builder.Constants
{
    public class MealParts
    {
        public const string Apetizer = "Soup";
        public const string Meat = "Chiken";
        public const string Salad = "Tomato salad";
        public const string Garnish = "Rice";
        public const string Drink = "Lemon water";
        public const string Dessert = "Cheesecake";

        public static List<string> AllMealParts
        {
            get
            {
                 return new List<string>
                {
                    Apetizer,
                    Meat,
                    Salad,
                    Garnish,
                    Drink,
                    Dessert
                };
            }
        }
    }
}
