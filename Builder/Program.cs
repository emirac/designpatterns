﻿using Builder.Classes;
using System;

namespace Builder
{
    class Program
    {
        static void Main(string[] args)
        {
            MealDirector director = new MealDirector();
            Meal mainCourse = director.BuildMainCourse();
            FormatResults(mainCourse);
            Meal fullMeal = director.BuildFullMeal();
            FormatResults(fullMeal);
            Console.ReadLine();
        }

        public static void FormatResults(Meal meal)
        {
            Console.WriteLine($"This meal consists of:");
            meal.Items.ForEach(Console.WriteLine);
            Console.WriteLine();
        }
    }
}
