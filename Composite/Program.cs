﻿using Composite.Classes;
using System;

namespace Composite
{
    class Program
    {
        static void Main(string[] args)
        {
            Department department = new Department();
            Team team1 = new Team();
            Employee emp1 = new Employee("Simple employee");
            Employee emp2 = new Employee("Simple employee");
            team1.TeamMembers.Add(emp1);
            team1.TeamMembers.Add(emp2);

            Team team2 = new Team();
            Employee emp3 = new Employee("Simple employee");
            Employee emp4 = new Employee("Simple employee");
            team2.TeamMembers.Add(emp3);
            team2.TeamMembers.Add(emp4);

            department.Teams.Add(team1);
            department.Teams.Add(team2);

            department.Work();
            foreach (Team t in department.Teams)
            {
                t.Work();
                t.TeamMembers.ForEach(m => m.Work());
            }

            Console.ReadLine();
        }
    }
}
