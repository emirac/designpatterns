﻿namespace Composite.Interfaces
{
    public interface IEmployee
    {
        void Work();
    }
}
