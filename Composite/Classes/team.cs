﻿using Composite.Interfaces;
using System;
using System.Collections.Generic;

namespace Composite.Classes
{
    public class Team : IEmployee
    {
        public List<Employee> TeamMembers { get; } = new List<Employee>();
        public Employee TeamLead { get; set; }

        public Team()
        {
            this.TeamLead = new Employee("Team Lead");
        }

        public void Work()
        {
            Console.WriteLine("Team is working");
        }
    }
}
