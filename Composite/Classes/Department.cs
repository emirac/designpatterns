﻿using Composite.Interfaces;
using System;
using System.Collections.Generic;

namespace Composite.Classes
{
    public class Department : IEmployee
    {
        public List<Team> Teams { get; } = new List<Team>();
        public Employee HeadOfDepartment { get; set; }

        public Department()
        {
            this.HeadOfDepartment = new Employee("Head of Department");
        }

        public void Work()
        {
            Console.WriteLine("Department is working");
        }
    }
}
