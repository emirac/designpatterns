﻿using Composite.Interfaces;
using System;

namespace Composite.Classes
{
    public class Employee : IEmployee
    {
        public string Position { get; set; }

        public Employee()
        {

        }

        public Employee(string position)
        {
            this.Position = position;
        }

        public void Work()
        {
            Console.WriteLine($"{this.Position} is working");
        }
    }
}
