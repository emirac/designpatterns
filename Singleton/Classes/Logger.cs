﻿using System;

namespace Singleton.Classes
{
    public class Logger
    {
        private static Logger _instance;

        private Logger()
        {

        }

        public static Logger GetInstance()
        {
            if (_instance == null)
            {
                return new Logger();
            }
            return _instance;
        }

        public void LogMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}
