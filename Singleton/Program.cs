﻿using System;
using Singleton.Classes;

namespace Singleton
{
    class Program
    {
        static void Main(string[] args)
        {
            Logger l1 = Logger.GetInstance();
            Logger l2 = Logger.GetInstance();

            l1.LogMessage($"This is {l1.GetType().Name}");
            l2.LogMessage($"This is {l2.GetType().Name}");
            Console.WriteLine($"Objects are same instance: {l1.Equals(l1)}");
            Console.ReadLine();
        }
    }
}
